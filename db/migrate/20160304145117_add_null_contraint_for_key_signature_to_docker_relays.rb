class AddNullContraintForKeySignatureToDockerRelays < ActiveRecord::Migration[4.2]
  def change
    change_column_null :docker_relays, :key_signature, false
  end
end
