require 'securerandom'

class AddNameToMachines < ActiveRecord::Migration[4.2]
  def up
    add_column :machines, :name, :string
    add_index :machines, [:name, :organization_id], unique: true
    Machine.reset_column_information
    Machine.all.each do |m|
      m.update_column(:name, SecureRandom.uuid)
    end
  end

  def down
    remove_column :machines, :name
  end
end
