# frozen_string_literal: true

class SyncScanSchedulesJob
  include ActsAsRecurringJob

  class << self
    def job_action
      Rails.logger.info 'Checking for out of sync scan schedules...'
      machine_schedules, organization_schedules = partition_schedule_keys
      machine_schedules.each do |schedule|
        ScanScheduleValidator.new(MachineScanSchedule, schedule).validate
      end
      organization_schedules.each do |schedule|
        ScanScheduleValidator.new(OrganizationScanSchedule, schedule).validate
      end
      Rails.logger.info 'Out of sync scan schedule check complete!'
    end

    private

    def partition_schedule_keys
      Resque.schedule.keys.select { |k| k.to_s =~ /_scan_schedule_\d/ }.partition { |k| k.to_s.start_with?('machine') }
    end
  end
end

class ScanScheduleValidator
  attr_reader :schedule_klass, :schedule_name, :scan

  def initialize(schedule_klass, schedule_name)
    @schedule_klass = schedule_klass
    @schedule_name = schedule_name
    @scan = schedule_klass.find_by(id: schedule_name[/\d+\z/].to_i)
  end

  def validate
    return remove_schedule unless scan
    cron_string = scan.send(:cron_string)
    schedule = Resque.fetch_schedule(schedule_name)
    update_schedule if cron_string != schedule['cron']
  end

  private

  def update_schedule
    Rails.logger.info 'Discrepency in database and scan schedule found. Updating schedule.'
    scan.valid? ? scan.send(:update_scan_in_queue) : remove_schedule
  end

  def remove_schedule
    Rails.logger.info 'Discrepency in database and scan schedule found. Removing schedule.'
    Resque.remove_schedule schedule_name
  end
end
