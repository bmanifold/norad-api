# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::ResultExportQueuesController, type: :controller do
  include NoradControllerTestHelpers

  let(:org) { create :organization }

  describe 'GET #index' do
    %i[organization_admin organization_reader].each do |role|
      it "gets a list of result_export_queues for #{role}" do
        @_current_user.add_role role, org
        create_list :jira_export_queue, 2, organization: org
        norad_get :index, organization_id: org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('result_export_queues')
      end
    end
  end
end
