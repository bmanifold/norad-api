# frozen_string_literal: true

module NoradScanBuilder
  module ScanJobUtilities
    include NoradRelayHelpers

    def to_h
      relay_opts(organization).merge(
        container_secret: secret.secret,
        container_full_path: container_full_path
      )
    end
  end
end
