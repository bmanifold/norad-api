# frozen_string_literal: true

# == Schema Information
#
# Table name: authentication_methods
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_authentication_methods_on_type     (type)
#  index_authentication_methods_on_user_id  (user_id) UNIQUE
#

FactoryBot.define do
  factory :local_authentication_method do
    before(:create) do |l|
      create :local_authentication_record, local_authentication_method: l
    end
  end
end
