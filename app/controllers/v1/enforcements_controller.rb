# frozen_string_literal: true

module V1
  class EnforcementsController < V1::ApplicationController
    before_action :set_org, only: %i[index create]

    def index
      render json: @org.enforcements
    end

    def create
      enforcement = @org.enforcements.build(enforcement_params)
      if enforcement.save
        render json: enforcement
      else
        render json: { errors: enforcement.errors }
      end
    end

    def destroy
      enforcement = Enforcement.find(params[:id])
      authorize_action_for enforcement
      enforcement.destroy!
      head :no_content
    end

    private

    def set_org
      @org = Organization.find(params[:organization_id])
      authorize_action_for Enforcement, in: @org
    end

    def enforcement_params
      params.require(:enforcement).permit(:requirement_group_id)
    end
  end
end
