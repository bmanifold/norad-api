class EnableVaultEncryptionForResults < ActiveRecord::Migration[5.0]
  def up
    add_column :results, :title_encrypted, :string
    add_column :results, :description_encrypted, :string
    add_column :results, :output_encrypted, :string
    add_column :results, :nid_encrypted, :string
    Result.reset_column_information

    Result.all.each do |result|
      result.title = result.attributes['title']
      result.description = result.attributes['description']
      result.output = result.attributes['output']
      result.nid = result.attributes['nid']
      result.save!
    end

    remove_column :results, :title
    remove_column :results, :description
    remove_column :results, :output
    remove_column :results, :nid
  end

  def down
    remove_column :results, :title_encrypted
    remove_column :results, :description_encrypted
    remove_column :results, :output_encrypted
    remove_column :results, :nid_encrypted
    add_column :results, :title, :string
    add_column :results, :description, :string
    add_column :results, :output, :string
    add_column :results, :nid, :string
  end
end
