# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/scan_args'

RSpec.describe NoradScanBuilder::ServiceScanArgs, with_resque_doubled: true do
  it_should_behave_like 'a Scan Args class' do
    let(:options) { { service: build_stubbed(:service) } }
  end

  describe '#to_a' do
    let(:port) { rand(0..65_535) }
    let(:service) { create(:service, port: port) }
    let(:container) { create(:service_test) }
    let(:scan_args) { described_class.new(container, service: service) }

    it 'uses the machine associated with the service for the target' do
      expect(scan_args.to_a).to include(service.machine.target_address)
    end

    it 'populates the args array with the port of the service' do
      expect(scan_args.to_a).to include(port.to_s)
    end

    context 'for a configurable test' do
      let(:default_foo) { 'bar' }
      let(:prog_args) { '%{target} -p %{port} -x %{foo}' }
      let(:container) do
        create(:security_container, configurable: true, default_config: { foo: default_foo }, prog_args: prog_args)
      end

      it 'uses the machine test config if it exists' do
        create(:security_container_config,
               configurable: service.machine,
               values: { 'foo' => 'baz' },
               security_container: container)

        expect(scan_args.to_a).to include('baz')
        expect(scan_args.to_a).to_not include(default_foo)
      end

      it 'uses the default config if no machine test config exists' do
        expect(scan_args.to_a).to include(default_foo)
      end
    end
  end
end
