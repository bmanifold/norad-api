# frozen_string_literal: true

# This job simply serves to keep the Relay version cache hot.
class SyncRelayVersionJob < ApplicationJob
  include ActsAsRecurringJob

  class << self
    def job_action
      latest_version = RelayImageInfoFetcher.fetch_and_cache_latest_version
      return if latest_version.blank?
      refresh_stale_records(latest_version)
    end

    def refresh_stale_records(latest_version)
      DockerRelay.where.not(last_reported_version: nil).update_all(
        ['outdated = (last_reported_version<>?)', latest_version]
      )
    end
  end
end
