class AddErrableToOrganizationErrors < ActiveRecord::Migration[5.0]
  def change
    add_reference :organization_errors, :errable, polymorphic: true, index: false
    remove_index :organization_errors, :organization_id
    add_index :organization_errors,
              %i[organization_id errable_id errable_type type], # only one error of each type, errable per organization
              name: :index_organization_errors_on_o_id_and_type_and_e_type_and_e_id,
              unique: true
  end
end
