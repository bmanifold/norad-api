# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MachineShowSerializer do
  context '#enabled_tests_count' do
    it 'returns correct count' do
      machine = create :machine
      create(:security_container_config, machine: machine)
      create(:security_container_config, organization: machine.organization)

      expect(described_class.new(machine).enabled_tests_count).to eq(2)
    end

    it 'returns zero when there are zero enabled tests' do
      machine = create :machine

      expect(described_class.new(machine).enabled_tests_count).to eq(0)
    end
  end
end
