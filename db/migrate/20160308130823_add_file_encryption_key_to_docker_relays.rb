class AddFileEncryptionKeyToDockerRelays < ActiveRecord::Migration[4.2]
  def change
    add_column :docker_relays, :file_encryption_key, :string, null: false
  end
end
