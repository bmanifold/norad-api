# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DockerSwarm do
  before :all do
    RSpec.configure { |c| c.double_docker = false }
  end

  after :all do
    RSpec.configure { |c| c.double_docker = true }
  end

  context 'class methods' do
    before :each do
      @container_instance_double = instance_double(Docker::Container)
      allow(@container_instance_double).to receive(:start)
      allow(Docker::Container).to receive(:create).and_return(@container_instance_double)
    end

    shared_examples_for 'a queued container' do
      context 'without relay' do
        let(:expected_args) do
          base_args.merge(
            NetworkMode: '',
            Dns: []
          )
        end

        it 'calls Docker::Container.create with proper args' do
          expect(Docker::Image).to receive(:create).with(fromImage: image)
          expect(Docker::Container).to receive(:create).with(expected_args)
          expect(Publisher).not_to receive(:publish)
          without_relay_proc.call
        end
      end

      context 'with relay' do
        let(:key) { 'key' }
        let(:queue) { 'queue' }
        let(:exchange) { 'exchange' }
        let(:relay_opts) do
          {
            relay_secret: key,
            relay_queue: queue,
            relay_exchange: exchange
          }
        end
        let(:expected_args) do
          [
            exchange,
            { docker_options: base_args.merge('encoded_relay_private_key' => key) },
            queue
          ]
        end

        it 'calls Publisher.publish with proper args' do
          expect(Docker::Image).not_to receive(:create)
          expect(Docker::Container).not_to receive(:create)
          expect(Publisher).to receive(:publish).with(*expected_args)
          with_relay_proc.call
        end
      end
    end

    describe '#queue_container' do
      it_behaves_like 'a queued container' do
        let(:name) { 'image-name' }
        let(:image) { 'foobar-registry.cisco.com:5000/name:tag' }
        let(:args) { { arg1: 'arg' } }
        let(:targets) { '127.0.0.1' }
        let(:secret) { create(:security_container_secret).secret }
        let(:env) do
          [
            'NORAD_ROOT=http://172.17.0.1:3000',
            'ASSESSMENT_PATHS="127.0.0.1"',
            "NORAD_SECRET=#{secret}"
          ]
        end
        let(:base_args) do
          {
            'name' => name,
            'Image' => image,
            'Env' => env,
            'Cmd' => args
          }
        end
        let(:without_relay_proc) do
          proc do
            DockerSwarm.queue_container(name, image, args, targets, secret)
          end
        end
        let(:with_relay_proc) do
          proc do
            DockerSwarm.queue_container(name, image, args, targets, secret, relay_opts)
          end
        end
      end
    end

    context '#queue_discovery_container' do
      it_behaves_like 'a queued container' do
        let(:discovery) do
          allow(DockerSwarm).to receive(:queue_machine_connectivity_container)
          create(:service_discovery)
        end
        let(:image) { 'norad-registry.cisco.com:5000/service-discovery:latest' }
        let(:env) do
          [
            'NORAD_ROOT=http://172.17.0.1:3000',
            "NORAD_SERVICES_URL=/v1/machines/#{discovery.machine.to_param}/services",
            "NORAD_DISCOVERY_URL=/v1/service_discoveries/#{discovery.to_param}",
            "NORAD_SECRET=#{discovery.shared_secret}"
          ]
        end
        let(:base_args) do
          {
            'Image' => image,
            'Env' => env,
            'Cmd' => [discovery.machine.target_address]
          }
        end
        let(:without_relay_proc) do
          proc do
            DockerSwarm.queue_discovery_container(discovery)
          end
        end
        let(:with_relay_proc) do
          proc do
            DockerSwarm.queue_discovery_container(discovery, relay_opts)
          end
        end
      end
    end

    context '#queue_machine_connectivity_container' do
      let(:before_proc) do
        proc do
          allow(Docker::Image).to receive(:create)
          machine = create(:machine)
          create(:service, port: 1, machine: machine)
          create(:service, port: 2, port_type: :udp, machine: machine)
          create(:ssh_service, port: 22, machine: machine)
          @assignment = create :ssh_key_pair_assignment, machine: machine
          @mcc = create container_name.underscore.to_sym, machine: machine.reload
        end
      end
      let(:check_path) do
        path_method = "v1_#{@mcc.class.name.underscore}_path"
        Rails.application.routes.url_helpers.send(path_method, @mcc)
      end
      let(:image) { "norad-registry.cisco.com:5000/#{container_name}:latest" }
      let(:env) do
        [
          'NORAD_ROOT=http://172.17.0.1:3000',
          "NORAD_CONNECTIVITY_CHECK_PATH=#{check_path}",
          "NORAD_SECRET=#{@mcc.shared_secret}"
        ]
      end
      let(:without_relay_proc) do
        proc do
          DockerSwarm.queue_machine_connectivity_container(@mcc)
        end
      end
      let(:with_relay_proc) do
        proc do
          DockerSwarm.queue_machine_connectivity_container(@mcc, relay_opts)
        end
      end

      it_behaves_like 'a queued container' do
        before(:each) { before_proc.call }
        let(:container_name) { 'ssh-connectivity-check' }
        let(:error_klass) { UnableToSshToMachineError }
        let(:base_args) do
          {
            'Image' => image,
            'Env' => env,
            'Cmd' => [
              @mcc.machine.target_address,
              @mcc.machine.ssh_key_values[:ssh_user],
              @mcc.machine.ssh_key_values[:ssh_key],
              @mcc.machine.ssh_services.pluck(:port).to_json
            ]
          }
        end
      end

      it_behaves_like 'a queued container' do
        before(:each) { before_proc.call }
        let(:container_name) { 'ping-connectivity-check' }
        let(:error_klass) { UnableToPingMachineError }
        let(:base_args) do
          {
            'Image' => image,
            'Env' => env,
            'Cmd' => [
              @mcc.machine.target_address,
              { tcp: [1, 22], udp: [2] }.to_json
            ]
          }
        end
      end
    end
  end
end
