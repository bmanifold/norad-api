# frozen_string_literal: true

# FIXME: IN GENERAL... handle the queue server being down
class Publisher
  MAX_RETRIES = 5
  @connection_params = { host: ENV['RABBITMQ_SERVER'], tls: !ENV['RABBITMQ_TLS'].nil? }
  @connection_params[:user] = ENV['RABBITMQ_API_USER'] if ENV['RABBITMQ_API_USER'].present?
  @connection_params[:password] = ENV['RABBITMQ_API_PASSWORD'] if ENV['RABBITMQ_API_PASSWORD'].present?
  class << self
    def publish(exchange_name, message, routing_key = 'default')
      Rails.logger.debug "Publishing message: #{message.inspect} to the #{exchange_name} exchange"
      x = channel.direct exchange_name, durable: true
      x.publish message.to_json, routing_key: routing_key
      true
    rescue NoMethodError => e
      Rails.logger.error "Unable to publish message: #{e.message}"
      false
    end

    def bind_queue(exchange_name, queue_name)
      retries ||= 0
      Rails.logger.info "Binding the #{queue_name} queue to the #{exchange_name} exchange"
      x = channel.direct exchange_name, durable: true
      q = channel.queue queue_name, durable: true, auto_delete: true
      q.bind x, routing_key: 'default'
      q.bind x, routing_key: queue_name
    rescue Bunny::ChannelAlreadyClosed, Bunny::NotFound
      @channel = nil
      (retries += 1) < MAX_RETRIES ? retry : raise
    end

    def channel
      @channel ||= connection.create_channel
    end

    def connection
      Rails.logger.debug 'Connecting to the queue server'
      @connection = Bunny.new(@connection_params).tap(&:start)
    rescue Bunny::TCPConnectionFailedForAllHosts, AMQ::Protocol::EmptyResponseError => e
      Rails.logger.error "Unable to connect to RabbitMQ server: #{e.message}"
      nil
    end
  end
end
