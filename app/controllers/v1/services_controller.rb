# frozen_string_literal: true

module V1
  class ServicesController < ApplicationController
    before_action :set_service, only: %i[show update destroy]
    before_action :set_machine, only: %i[create index]
    skip_before_action :require_api_token, only: :create
    before_action :require_api_token_or_signature, only: :create

    def index
      authorize_action_for Service, in: @machine.organization
      render json: @machine.services
    end

    def show
      render json: @service
    end

    def update
      return render(json: @service) if @service.update(service_update_params)
      render_errors_for(@service)
    end

    def create
      @service = @machine.services.build(service_creation_params)
      authorize_action_for Service, in: @machine.organization unless via_discovery?

      return render(json: @service) if @service.save
      render_errors_for(@service)
    end

    def destroy
      @service.destroy
      head :no_content
    end

    private

    def set_service
      @service = Service.find(params[:id])
      authorize_action_for @service
    end

    WEB_APPLICATION_CONFIG_SHARED_ATTRIBUTES = %i[
      auth_type
      starting_page_path
      login_form_username_field_name
      login_form_password_field_name
      url_blacklist
    ].freeze

    CREATE_AND_UPDATE_SHARED_ATTRIBUTES = [
      :name,
      :description,
      :port,
      :port_type,
      :encryption_type,
      :allow_brute_force,
      { application_type_attributes: %i[port name transport_protocol] }
    ].freeze

    def service_creation_params
      params.require(:service).permit(
        *(
          CREATE_AND_UPDATE_SHARED_ATTRIBUTES + %i[discovered] +
          [:type, { service_identity_attributes: %i[username password] }] +
          [{ web_application_config_attributes: WEB_APPLICATION_CONFIG_SHARED_ATTRIBUTES }]
        )
      )
    end

    def service_update_params
      params.require(:service).permit(
        *(
          CREATE_AND_UPDATE_SHARED_ATTRIBUTES +
          [{ service_identity_attributes: %i[id username password] }] +
          [{ web_application_config_attributes: WEB_APPLICATION_CONFIG_SHARED_ATTRIBUTES + %i[id] }]
        )
      )
    end

    def set_machine
      @machine = Machine.find(params[:machine_id])
    end

    def require_api_token_or_signature
      return require_api_token unless via_discovery?
      require_container_secret_signature(@machine.service_discoveries.current&.shared_secret || SecureRandom.hex)
    end

    def via_discovery?
      params[:service][:discovered].to_s == 'true'
    end
  end
end
