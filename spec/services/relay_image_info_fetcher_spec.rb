# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RelayImageInfoFetcher do
  before :each do
    RSpec.configure { |c| c.double_relay_image_info_fetcher = false }
  end

  after :each do
    RSpec.configure { |c| c.double_relay_image_info_fetcher = true }
  end

  context 'class methods' do
    describe '::catched_latest_version' do
      let(:manifest) do
        {
          'config' => { 'digest' => 'valid' }
        }
      end

      before :each do
        redis = double
        allow(redis).to receive(:get).with('relay_version')
        allow(redis).to receive(:setex)
        @resque_double = object_double('Resque').as_stubbed_const
        allow(@resque_double).to receive(:redis).and_return redis
        allow(DockerRegistry).to receive(:json_request).with('/relay/tags/list').and_return tags
        allow(DockerRegistry).to receive(:json_request).with(%r{/relay/manifests.*}).and_return manifest
      end

      context 'without latest' do
        let(:tags) { { 'tags' => %w[0.0.1 0.0.2 0.0.3 0.0.4 0.0.5] } }

        it 'returns the latest numerical version' do
          expect(described_class.fetch_and_cache_latest_version).to eq '0.0.5'
        end
      end

      context 'with matching latest' do
        let(:tags) { { 'tags' => %w[0.0.1 0.0.2 0.0.3 0.0.4 0.0.5 latest] } }

        it 'returns the latest numerical version' do
          expect(described_class.fetch_and_cache_latest_version).to eq '0.0.5'
        end
      end
    end
  end
end
