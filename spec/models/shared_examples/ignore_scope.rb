# frozen_string_literal: true

RSpec.shared_examples 'an Ignore Scope' do
  it { should have_many(:result_ignore_rules) }

  it 'defines a #latest_results method' do
    expect(subject).to respond_to(:latest_results)
  end
end
