class CreateUniqueIndexAcrossIgnoreScopeSignatureOnResultIgnoreRules < ActiveRecord::Migration[5.0]
  def change
    add_index :result_ignore_rules,
              [:ignore_scope_type, :ignore_scope_id, :signature],
              name: 'index_result_ignore_rules_on_i_s_type_and_i_s_id_and_signature',
              unique: true
  end
end
