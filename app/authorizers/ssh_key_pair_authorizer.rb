# frozen_string_literal: true

class SshKeyPairAuthorizer < ApplicationAuthorizer
  class << self
    def readable_by?(user, options)
      reader?(user, options[:in]) || admin?(user, options[:in])
    end

    def creatable_by?(user, options)
      admin?(user, options[:in])
    end
  end

  def readable_by?(user)
    reader?(user, org) || admin?(user, org)
  end

  def updatable_by?(user)
    admin?(user, org)
  end

  def deletable_by?(user)
    admin?(user, org)
  end

  private

  def org
    resource.organization
  end
end
