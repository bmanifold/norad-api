# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResultGenerator do
  describe 'instance methods' do
    let(:subject) { described_class.new(create(:assessment)) }

    # Known-good result signatures for errored and timed out results. If these fail, the implementation has changed and
    # all existing errored and timed out result signatures need to be updated for all organizations.
    let(:errored_api_hash) { 'c8ef54a3e8268ddc8ddacaaafdf35a081bfb7494a9a05c6cea18c90150b6b1fc' }
    let(:timed_out_hash) { 'd37402715d3017c0a8a05bd7f54cb8ffd0a98996bb0efaac95201d4de35e5c1c' }
    let(:errored_relay_hash) { 'a34d946268dece97a9c350c862184481ccfe259170cc374a3d6925acffbb061d' }

    it 'generates an api errored result' do
      expect { subject.create_errored_api_result! }.to change(Result, :count).by(1)
      expect(Result.last.signature).to eq errored_api_hash
    end

    it 'generates a timed out result' do
      expect { subject.create_timed_out_result! }.to change(Result, :count).by(1)
      expect(Result.last.signature).to eq timed_out_hash
    end

    it 'generates a relay errored result' do
      expect { subject.create_errored_relay_result! }.to change(Result, :count).by(1)
      expect(Result.last.signature).to eq errored_relay_hash
    end
  end
end
