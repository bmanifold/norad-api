# frozen_string_literal: true

require 'active_job/logging'

module ActiveJob
  module Logging
    class LogSubscriber < ActiveSupport::LogSubscriber
      private

      def args_info(job)
        if job.arguments.any?
          ' with arguments: <arguments removed by initializer in Norad API>'
        else
          ''
        end
      end
    end
  end
end
