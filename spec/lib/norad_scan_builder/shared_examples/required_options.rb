# frozen_string_literal: true

RSpec.shared_examples 'Norad Scan Builder Required Options' do
  let(:options) { {} }

  it 'requires the options to be present' do
    options.keys.each do |opt|
      expect do
        # Hash#except is provided by ActiveSupport, not available in vanilla Ruby
        described_class.new(double, double, double, options.except(opt))
      end.to raise_exception(KeyError)
    end
  end
end
