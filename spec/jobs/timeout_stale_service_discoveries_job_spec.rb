# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe TimeoutStaleServiceDiscoveriesJob, type: :job, with_resque_doubled: true do
  before :each do
    allow(@resque_module_double).to receive(:logger)
  end

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'

  it 'looks for stale Service Discoveries to time out' do
    expect(ServiceDiscovery).to receive(:stale).and_call_original
    perform_enqueued_jobs { job }
  end

  it 'sends the fail! message to the return value of ServiceDiscovery.stale' do
    sd = double('A stale service discovery')
    expect(ServiceDiscovery).to receive(:stale).and_return([sd])
    expect(sd).to receive(:error_message=)
    expect(sd).to receive(:fail!)
    perform_enqueued_jobs { job }
  end
end
