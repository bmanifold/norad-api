# frozen_string_literal: true

ActiveModelSerializers.config.adapter = :json

# To emulate RocketPants behavior, we'll set the root key to always be "response"
module ActiveModelSerializers
  module Adapter
    class Base
      private

      def root
        'response'
      end
    end
  end
end
