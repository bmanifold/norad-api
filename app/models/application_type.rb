# frozen_string_literal: true

# == Schema Information
#
# Table name: application_types
#
#  id                 :integer          not null, primary key
#  port               :integer          not null
#  name               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  transport_protocol :integer          default("tcp"), not null
#
# Indexes
#
#  index_application_types_on_port  (port)
#

class ApplicationType < ApplicationRecord
  has_many :services, inverse_of: :application_type
  has_many :security_containers, inverse_of: :application_type

  enum transport_protocol: { tcp: 0, udp: 1 }
end
