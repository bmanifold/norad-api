# frozen_string_literal: true

# == Schema Information
#
# Table name: docker_commands
#
#  id                    :integer          not null, primary key
#  error_details         :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  machine_id            :integer
#  organization_id       :integer
#  state                 :integer          default("pending"), not null
#  assessments_in_flight :integer          default(0)
#  started_at            :datetime
#  finished_at           :datetime
#
# Indexes
#
#  index_docker_commands_on_machine_id       (machine_id)
#  index_docker_commands_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_0545a1ce1d  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_b85f190c31  (machine_id => machines.id) ON DELETE => cascade
#

class DockerCommandSerializer < ActiveModel::Serializer
  attributes(
    :id,
    :error_details,
    :machine_id,
    :organization_id,
    :state,
    :created_at,
    :updated_at,
    :state_details
  )

  has_many :assessments
  has_many :security_containers

  def assessments
    return object.assessments unless instance_options[:machine_filter_id]
    object.assessments.where(machine_id: instance_options[:machine_filter_id])
  end

  def state_details
    DockerCommandDecorator.new(object).state_details
  end
end
