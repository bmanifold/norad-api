# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe RelayQueueError, type: :model do
  context 'validations' do
    it { should validate_presence_of(:errable) }
  end

  describe '#message' do
    let(:relay) { build_stubbed(:docker_relay) }
    let(:error) { build_stubbed(:relay_queue_error, errable: relay) }

    it 'should return the error message scoped to the associated Relay' do
      expect(error.message).to eq(
        "Relay #{relay.id} cannot connect to the AMQP queue! Check the Relay's logs for more information."
      )
    end
  end
end
