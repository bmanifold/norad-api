class CreateLocalAuthenticationRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :local_authentication_records do |t|
      t.string :password_digest
      t.string :password_reset_token
      t.datetime :password_reset_sent_at
      t.integer :authentication_method_id
      t.string :email_confirmation_token
      t.datetime :email_confirmed_at

      t.timestamps
    end
    add_index :local_authentication_records, :password_reset_token, unique: true
    add_index :local_authentication_records, :authentication_method_id, unique: true
    add_index :local_authentication_records, :password_reset_sent_at
    add_index :local_authentication_records, :email_confirmation_token, unique: true
    add_index :local_authentication_records, :email_confirmed_at
  end
end
