# frozen_string_literal: true

RSpec.shared_examples 'a Scan Args class' do
  let(:options) { {} }
  let(:scan_args) { described_class.new(build_stubbed(:security_container), options) }

  describe 'instantion' do
    it 'requires the options to be present' do
      options.keys.each do |opt|
        expect do
          # Hash#except is provided by ActiveSupport, not available in vanilla Ruby
          described_class.new(double, options.except(opt))
        end.to raise_exception(KeyError)
      end
    end
  end

  describe 'instance methods' do
    it 'responds to :to_a' do
      expect(scan_args.respond_to?(:to_a)).to eq(true)
    end

    describe 'attribute readers' do
      it 'responds_to :container' do
        expect(scan_args.respond_to?(:container)).to be(true)
      end
    end
  end

  describe 'exception handling when building the args array' do
    it 'catches exceptions and returns an empty array' do
      allow(scan_args).to receive(:args_array).and_raise(StandardError)
      expect(scan_args.to_a).to eq([])
    end
  end
end
