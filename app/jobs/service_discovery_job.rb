# frozen_string_literal: true

require './lib/norad_exception_notifier'
require './lib/norad_relay_helpers'

class ServiceDiscoveryJob < ApplicationJob
  include NoradExceptionNotifier
  include NoradRelayHelpers

  queue_as :swarm_scheduling

  rescue_from(StandardError) do |exception|
    notify_airbrake(exception)
    Resque.logger.error "Exception while discovering assets: #{exception.inspect}" if defined?(Resque)
    # XXX: The first argument passed to the perform method is the discovery object.
    discovery = @arguments.first
    ActiveRecord::Base.transaction do
      discovery.error_message = 'Failed to schedule service discovery'
      discovery.fail!
    end
  end

  after_perform do |job|
    discovery = job.arguments.first
    discovery.start!
  end

  def perform(discovery)
    DockerSwarm.queue_discovery_container(discovery, relay_opts(discovery.organization))
  end
end
