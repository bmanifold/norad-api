# frozen_string_literal: true

require 'open-uri'
require 'base64'

# This class interacts with a Docker Registry API
class DockerRegistry
  class << self
    def json_request(endpoint)
      response_body = req(base_uri + endpoint).read
      JSON.parse(response_body)
    end

    private

    def base_uri
      "https://#{NORAD_REGISTRY}/v2"
    end

    def accept_header
      { 'Accept' => 'application/vnd.docker.distribution.manifest.v2+json' }
    end

    def token_header
      { 'Authorization' => @authorization }
    end

    # The below OAuth token generation approach for the Docker Registry v2 API is losely based on
    # https://github.com/deitch/docker_registry2
    # Modifications were made to make the code simpler and easier to read
    # It basically goes like:
    # 1. Fetch the resource
    # 2. Rescue Unauthorized, get Bearer token
    # 3. Retry request with new token
    def req(uri)
      headers = accept_header.merge(token_header)
      open(uri, headers)
    rescue OpenURI::HTTPError => e
      status_code, _status_human = e.io.status
      raise unless status_code == '401' # Unauthorized

      # Attempt authentication to realm
      header = e.io.meta['www-authenticate']
      build_http_auth_from_header(header)
      retry
    end

    # At this point, the header looks like (for production):
    # Bearer realm="https://norad-registry.cisco.com:5001/auth",service="Docker Registry",
    #   scope="repository:relay:pull"
    #
    #   or (for development):
    #
    # Basic realm="Registry Realm"
    def build_http_auth_from_header(header)
      strategy = header.split.first
      auth =
        # Use Bearer auth in production
        if strategy == 'Bearer' || Rails.env.production?
          authenticate_bearer(header)

        # Use Basic auth otherwise
        else
          creds = "#{ENV.fetch('REGISTRY_USERNAME')}:#{ENV.fetch('REGISTRY_PASSWORD')}"
          Base64.strict_encode64 creds
        end

      @authorization = "#{strategy} #{auth}"
    end

    # We make a request with these params to get a token back
    def authenticate_bearer(header)
      realm = parse_param_from_header('realm', header)
      service = parse_param_from_header('service', header)
      scope = parse_param_from_header('scope', header)
      query = "?service=#{service}&scope=#{scope}"

      response_body = open(realm + query, accept_header).read
      JSON.parse(response_body)['token']
    end

    def parse_param_from_header(param, header)
      header.scan(/#{param}="([^"]*)/).last.first
    end
  end
end
