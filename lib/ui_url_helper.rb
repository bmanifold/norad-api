# frozen_string_literal: true

class UiUrlHelper
  class << self
    def ui_url(url)
      uri = URI.parse(url)
      uri.path.sub!(%r{^/v1}, '')
      uri.to_s
    end
  end
end
