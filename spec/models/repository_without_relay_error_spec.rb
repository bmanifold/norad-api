# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'aasm/rspec'
require 'models/shared_examples/organization_errors'

RSpec.describe RepositoryWithoutRelayError, type: :model do
  it_behaves_like 'an Organization Error class'

  before :each do
    allow_any_instance_of(RepositoryWhitelistEntry).to receive(:check_for_organization_errors)
    allow_any_instance_of(DockerRelay).to receive(:check_for_organization_errors)
  end

  describe '::check method' do
    before :each do
      @organization = create :organization
      @repository = create :security_test_repository
    end

    describe 'error creation' do
      context 'when relay is unconfigured' do
        it 'sets an error when repository is enabled' do
          @repository.repository_whitelist_entries.create(organization: @organization)
          described_class.check(@organization)
          expect(@organization.organization_errors).not_to be_empty
        end

        it 'does not set an error when repository is not enabled' do
          described_class.check(@organization)
          expect(@organization.organization_errors).to be_empty
        end
      end
    end

    describe 'error removal' do
      before :each do
        # create error
        @entry = @repository.repository_whitelist_entries.create(organization: @organization)
        described_class.check(@organization)
      end

      it 'removes an error when repository is disabled' do
        expect(@organization.organization_errors).not_to be_empty
        @entry.destroy
        @organization.reload
        described_class.check(@organization)
        expect(@organization.organization_errors).to be_empty
      end

      it 'removes an error when repository is destroyed' do
        expect(@organization.organization_errors).not_to be_empty
        @repository.destroy
        @organization.reload
        described_class.check(@organization)
        expect(@organization.organization_errors).to be_empty
      end

      it 'removes an error when relay is configured' do
        expect(@organization.organization_errors).not_to be_empty
        relay = create :docker_relay, organization: @organization
        relay.verified = true
        relay.save

        @organization.reload
        described_class.check(@organization)
        expect(@organization.organization_errors).to be_empty
      end
    end
  end
end
