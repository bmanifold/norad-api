# frozen_string_literal: true

# These are used in container queries
module ContainerPathHelpers
  extend ActiveSupport::Concern

  included do
    private

    # Grab the server and port part of the container full path
    def repository_hosts(full_paths)
      Array(full_paths).collect do |path|
        if path.to_s.include? '/'
          path.to_s.split('/').first
        else
          ''
        end
      end
    end

    # Grab the image and tag from the container full path
    def container_names(full_paths)
      Array(full_paths).collect { |path| path.to_s.split('/').last.to_s }
    end

    def containers_from_unofficial_repos?(full_paths)
      SecurityTestRepository
        .where(official: false)
        .where('host IN (?)', repository_hosts(full_paths))
        .exists?
    end
  end
end
