# frozen_string_literal: true

require './lib/ui_url_helper'

class ApplicationMailer < ActionMailer::Base
  SUBJECT_PREFIX = '[Norad Notification]'
  default from: 'no-reply@norad.cisco.com'
end
