class Memberships < ActiveRecord::Migration[4.2]
  def change
    create_table :memberships do |t|
      t.references :user, null: false, index: true
      t.references :organization, null: false, index: true

      t.timestamps null: false
    end
    add_index :memberships, [:user_id, :organization_id], unique: true
    add_foreign_key :memberships, :users, on_delete: :cascade
    add_foreign_key :memberships, :organizations, on_delete: :cascade
  end
end
