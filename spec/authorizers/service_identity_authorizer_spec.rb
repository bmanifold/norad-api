# frozen_string_literal: true

require 'rails_helper'

describe ServiceIdentityAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @unprivileged_user = create :user
    @unprivileged_user_other = create :user
    @admin_user_other = create :user
  end

  context 'service action' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      machine1 = create :machine, organization: @org1
      @admin_user.add_role :organization_admin, @org1
      @admin_user_other.add_role :organization_admin, @org2
      @unprivileged_user.add_role :organization_reader, @org1
      @unprivileged_user_other.add_role :organization_reader, @org2
      service = create :web_application_service, machine: machine1
      @identity = create :service_identity, service_id: service.id
      @options = { in: machine1.organization }
    end

    it 'try to create' do
      expect(ServiceIdentity.authorizer).to be_creatable_by(@admin_user, @options)
      expect(ServiceIdentity.authorizer).to_not be_creatable_by(@admin_user_other, @options)
      expect(ServiceIdentity.authorizer).to_not be_creatable_by(@unprivileged_user, @options)
      expect(ServiceIdentity.authorizer).to_not be_creatable_by(@unprivileged_user_other, @options)
    end

    it 'try to delete' do
      expect(@identity.authorizer).to be_deletable_by(@admin_user)
      expect(@identity.authorizer).to_not be_deletable_by(@admin_user_other)
      expect(@identity.authorizer).to_not be_deletable_by(@unprivileged_user)
      expect(@identity.authorizer).to_not be_deletable_by(@unprivileged_user_other)
    end

    it 'try to update' do
      expect(@identity.authorizer).to be_updatable_by(@admin_user)
      expect(@identity.authorizer).to_not be_updatable_by(@admin_user_other)
      expect(@identity.authorizer).to_not be_updatable_by(@unprivileged_user)
      expect(@identity.authorizer).to_not be_updatable_by(@unprivileged_user_other)
    end

    it 'try to read from class level' do
      expect(ServiceIdentity.authorizer).to_not be_readable_by(@admin_user, @options)
      expect(ServiceIdentity.authorizer).to_not be_readable_by(@admin_user_other, @options)
      expect(ServiceIdentity.authorizer).to_not be_readable_by(@unprivileged_user, @options)
      expect(ServiceIdentity.authorizer).to_not be_readable_by(@unprivileged_user_other, @options)
    end

    it 'try to read from instance level' do
      expect(@identity.authorizer).to be_readable_by(@admin_user)
      expect(@identity.authorizer).to_not be_readable_by(@admin_user_other)
      expect(@identity.authorizer).to be_readable_by(@unprivileged_user)
      expect(@identity.authorizer).to_not be_readable_by(@unprivileged_user_other)
    end
  end
end
