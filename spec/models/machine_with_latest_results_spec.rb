# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples/ignore_scope'

RSpec.describe MachineWithLatestResults do
  let(:organization) { create :organization }
  let(:machine) { create(:machine, organization: organization) }
  let(:sectest) { create :security_container }
  let(:sc_attrs) { [{ security_container_id: sectest.id }] }

  it 'returns all results for the most recent command via #latest_results' do
    command = create :docker_command, commandable: machine, scan_containers_attributes: sc_attrs
    assessment = create :assessment, docker_command: command, machine: machine
    result = create :result, assessment: assessment
    allow(machine).to receive(:latest_command).and_return(command)
    decorated_machine = described_class.new(machine)

    expect(decorated_machine.latest_results).to match_array(Result.where(id: result))
  end

  it 'handles the case that no docker commands have been created for the machine' do
    command = create :docker_command, commandable: organization, scan_containers_attributes: sc_attrs
    assessment = create :assessment, docker_command: command, machine: machine
    result = create :result, assessment: assessment
    allow(machine).to receive(:latest_command).and_return(command)
    decorated_machine = described_class.new(machine)

    expect(decorated_machine.latest_results).to match_array(Result.where(id: result))
  end
end
