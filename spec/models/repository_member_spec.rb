# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_members
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_repository_members_on_security_test_repository_id  (security_test_repository_id)
#  index_repository_members_on_u_id_and_s_t_r_id            (user_id,security_test_repository_id) UNIQUE
#  index_repository_members_on_user_id                      (user_id)
#
# Foreign Keys
#
#  fk_rails_2a2cfda3a3  (user_id => users.id) ON DELETE => cascade
#  fk_rails_e2d371a88f  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe RepositoryMember, type: :model do
  let(:user) { create :user }
  let(:repository) { create :private_repository }

  context 'when maintaining associations' do
    it { should belong_to :user }
    it { should belong_to :security_test_repository }
  end

  context 'when validating' do
    it do
      create :repository_reader
      should validate_uniqueness_of(:user).scoped_to(:security_test_repository_id)
    end
  end

  context 'when creating admins' do
    let(:admin) { create :repository_admin, user: user, security_test_repository: repository }

    it 'an admin is created' do
      expect(admin.user.roles[-1].name).to eq('security_test_repository_admin')
      expect(admin.user.roles[-1].resource_type).to eq('SecurityTestRepository')
      expect(admin.user.roles[-1].resource_id).to eq(repository.id)
    end

    it 'each membership can have only one role' do
      new_reader = described_class.new(user: admin.user, security_test_repository: admin.security_test_repository)
      expect { new_reader.save! }.to raise_error ActiveRecord::RecordInvalid
    end
  end

  context 'when creating readers' do
    let(:reader) { create :repository_reader, user: user, security_test_repository: repository }

    it 'a reader is created' do
      expect(reader.user.roles[-1].name).to eq('security_test_repository_reader')
      expect(reader.user.roles[-1].resource_type).to eq('SecurityTestRepository')
      expect(reader.user.roles[-1].resource_id).to eq(repository.id)
    end

    it 'each membership can have only one role' do
      new_admin = described_class.new(user: reader.user, security_test_repository: reader.security_test_repository)
      expect { new_admin.save! }.to raise_error ActiveRecord::RecordInvalid
    end
  end

  context 'when creating invalid role' do
    it 'should error' do
      expect do
        create :repository_member, role_type: :invalid
      end.to raise_exception ActiveRecord::RecordNotSaved
    end
  end

  context 'when detroying members' do
    let(:repository2) { create :private_repository }

    before :each do
      @member1 = create :repository_admin, user: user, security_test_repository: repository
      @member2 = create :repository_admin, user: user, security_test_repository: repository2
    end

    it 'destroy only the first member' do
      expect { @member1.destroy! }.to change(described_class, :count).by(-1)
    end

    it 'destroy only the second member' do
      expect { @member2.destroy! }.to change(described_class, :count).by(-1)
    end
  end

  context 'when destroying reader roles' do
    before :each do
      @member = create :repository_reader, user: user, security_test_repository: repository
    end

    it 'removes all roles associated' do
      expect { @member.destroy! }.to change(RepositoryMember, :count).by(-1).and(change(Role, :count).by(-1))
      expect(user.has_role?(:security_test_repository_reader, repository)).to be_falsey
    end
  end

  context 'when destroying admin roles' do
    before :each do
      @member = create :repository_admin, user: user, security_test_repository: repository
    end

    it 'removes all roles associated' do
      expect { @member.destroy! }.to change(RepositoryMember, :count).by(-1).and(change(Role, :count).by(-1))
      expect(user.has_role?(:security_test_repository_admin, repository)).to be_falsey
    end
  end
end
