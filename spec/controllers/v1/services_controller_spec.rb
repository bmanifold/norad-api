# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::ServicesController, type: :controller do
  describe 'GET #index' do
    before :each do
      @machine = create :machine
      @service = create :service, machine: @machine
    end

    context 'as an organization admin' do
      it 'returns the services associated with a machine' do
        @_current_user.add_role :organization_admin, @machine.organization
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
      end
    end

    context 'as an organization reader' do
      it 'returns the services associated with a machine' do
        @_current_user.add_role :organization_reader, @machine.organization
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of services for a machine' do
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @machine = create :machine
      @service = create :service, machine: @machine
    end

    context 'as an organization admin' do
      it 'returns the details of a service' do
        @_current_user.add_role :organization_admin, @machine.organization
        norad_get :show, id: @service.id
        expect(response.status).to eq(200)
      end
    end

    context 'as an organization reader' do
      it 'read the details of a specific service' do
        @_current_user.add_role :organization_reader, @machine.organization
        norad_get :show, id: @service.id
        expect(response.status).to eq(200)
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the details of a specific service' do
        norad_get :show, id: @service.id
        expect(response.status).to eq(403)
      end
    end
  end

  RSpec::Matchers.define :rollback_db_changes_on_service_create_failure do |machine|
    match do |actual|
      expect do
        actual.call
      end.to change(
        WebApplicationConfig,
        :count
      ).by(0).and change(
        machine.services,
        :count
      ).by(0).and change(
        ServiceIdentity,
        :count
      ).by(0)
    end

    def supports_block_expectations?
      true
    end
  end

  describe 'POST #create' do
    let(:machine) { create :machine }
    let(:machine_without_permissions) { create :machine }
    let(:application_type) { create :application_type }

    let(:valid_configured_service_params) do
      {
        service: attributes_for(:web_application_service, machine: nil).merge(
          web_application_config_attributes: attributes_for(:web_application_config),
          service_identity_attributes: attributes_for(:service_identity),
          application_type_attributes: {
            name: application_type.name,
            port: application_type.port,
            transport_protocol: application_type.transport_protocol
          }
        ),
        machine_id: machine.to_param
      }
    end

    let(:invalid_configured_service_params) do
      {
        service: attributes_for(:web_application_service, machine: nil).merge(
          web_application_config_attributes: attributes_for(:web_application_config, starting_page_path: ' %'),
          service_identity_attributes: attributes_for(:service_identity)
        ),
        machine_id: machine.to_param
      }
    end

    before do
      create :application_type, name: 'ftp', port: 21, transport_protocol: 'tcp'
      create :application_type, name: 'https', port: 443, transport_protocol: 'tcp'
    end

    context 'as an organization admin' do
      before do
        @_current_user.add_role :organization_admin, machine.organization
      end

      it 'figures out application_type when application_type_attributes is not provided' do
        create_params = valid_configured_service_params
        create_params[:service].delete :application_type_attributes
        application_type = create(
          :application_type,
          port: valid_configured_service_params[:service][:port],
          transport_protocol: valid_configured_service_params[:service][:port_type]
        )
        norad_post :create, create_params
        service = WebApplicationService.find(response_body['response']['id'])

        expect(service.application_type).to eq(application_type)
      end

      it 'creates a new service for a machine when web_application_config, service_identity blank' do
        create_params = valid_configured_service_params
        create_params[:service].delete :web_application_config_attributes
        create_params[:service].delete :service_identity_attributes

        expect { norad_post :create, create_params }.to change(
          machine.services,
          :count
        ).by(1).and change(
          WebApplicationConfig,
          :count
        ).by(0).and change(
          ServiceIdentity,
          :count
        ).by(0)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        Service.find(response_body['response']['id'])
      end

      it 'creates a new configured service for a machine' do
        expect do
          norad_post :create, valid_configured_service_params
        end.to change(
          machine.services,
          :count
        ).by(1).and change(
          WebApplicationConfig,
          :count
        ).by(1).and change(
          ServiceIdentity,
          :count
        ).by(1)

        service = WebApplicationService.find(response_body['response']['id'])

        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        expect(service.name).to eq(valid_configured_service_params[:service][:name])
        expect(service.web_application_config.starting_page_path).to eq(
          valid_configured_service_params[:service][:web_application_config_attributes][:starting_page_path]
        )
        expect(service.service_identity.username).to eq(
          valid_configured_service_params[:service][:service_identity_attributes][:username]
        )
        expect(service.service_identity.password).to eq(
          valid_configured_service_params[:service][:service_identity_attributes][:password]
        )
        expect(service.application_type.name).to eq(application_type.name)
      end

      it 'rolls back any db changes if any part of the creation process fails' do
        expect do
          norad_post :create, invalid_configured_service_params
        end.to rollback_db_changes_on_service_create_failure(machine)

        assert_response 422
        expect(response_body['errors']['web_application_config.starting_page_path']).to eq(['must be a valid path'])
      end
    end

    context 'as an organization reader' do
      it 'cannot create a new service for a machine' do
        @_current_user.add_role :organization_reader, machine.organization

        expect do
          norad_post :create, valid_configured_service_params
        end.to rollback_db_changes_on_service_create_failure(machine)

        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create a new service for a machine' do
        @_current_user.add_role :organization_admin, machine_without_permissions.organization

        expect do
          norad_post :create, valid_configured_service_params
        end.to rollback_db_changes_on_service_create_failure(machine)

        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    let(:machine) { create :machine }
    let(:machine_without_permissions) { create :machine }
    let(:application_type) { create :application_type }

    let(:valid_configured_service_params) do
      {
        service: attributes_for(:web_application_service, machine: nil).merge(
          web_application_config_attributes: attributes_for(:web_application_config),
          service_identity_attributes: attributes_for(:service_identity),
          application_type_attributes: {
            name: application_type.name,
            port: application_type.port,
            transport_protocol: application_type.transport_protocol
          }
        ),
        machine_id: machine.to_param
      }
    end

    let(:invalid_configured_service_params) do
      {
        service: attributes_for(:web_application_service, machine: nil).merge(
          web_application_config_attributes: attributes_for(:web_application_config, starting_page_path: ' %'),
          service_identity_attributes: attributes_for(:service_identity)
        ),
        machine_id: machine.to_param
      }
    end

    let(:service) { create :web_application_service, machine: machine }
    let!(:config) { create :web_application_config, web_application_service: service }
    let!(:identity) { create :service_identity, service: service }
    let(:new_name) { "new_name_#{rand}" }
    let(:new_username) { "user-#{SecureRandom.hex}" }
    let(:new_path) { "/#{SecureRandom.hex}" }

    before do
      valid_configured_service_params[:service][:name] = new_name
      valid_configured_service_params[:id] = service.id
      valid_configured_service_params[:service][:web_application_config_attributes][:id] = config.id
      valid_configured_service_params[:service][:web_application_config_attributes][:starting_page_path] = new_path

      valid_configured_service_params[:service][:service_identity_attributes][:id] = identity.id
      valid_configured_service_params[:service][:service_identity_attributes][:username] = new_username

      invalid_configured_service_params[:service][:name] = new_name
      invalid_configured_service_params[:id] = service.id
      invalid_configured_service_params[:service][:web_application_config_attributes][:id] = config.id
      invalid_configured_service_params[:service][:web_application_config_attributes][:starting_page_path] = ' %'

      invalid_configured_service_params[:service][:service_identity_attributes][:id] = identity.id
      invalid_configured_service_params[:service][:service_identity_attributes][:username] = new_username
    end

    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, machine.organization
      end

      it 'ensures created services have unique ports' do
        dup_service = create :web_application_service, machine: machine
        invalid_params = valid_configured_service_params
        invalid_params[:service][:port] = dup_service.port

        norad_put :update, invalid_params

        assert_response 422
        expect(response_body['errors']['port']).to eq(['has already been taken'])
      end

      it 'updates a service for a machine when web_application_config, service_identity blank' do
        expect(service.name).to_not eq(new_name)
        update_params = valid_configured_service_params
        update_params[:service].delete :web_application_config_attributes
        update_params[:service].delete :service_identity_attributes

        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')

        service.reload

        expect(service.name).to eq(new_name)
        expect(service.application_type.name).to eq(application_type.name)
      end

      it 'updates a service for a machine' do
        expect(service.name).to_not eq(new_name)

        norad_put :update, valid_configured_service_params

        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')

        expect(service.reload.name).to eq(new_name)
        expect(service.reload.application_type.name).to eq(application_type.name)
        expect(config.reload.starting_page_path).to eq(new_path)
        expect(identity.reload.username).to eq(new_username)
      end

      it 'rolls back any db changes if any part of the update process fails' do
        service_name = service.name
        config_starting_page_path = config.starting_page_path
        identity_username = identity.username

        norad_put :update, invalid_configured_service_params

        expect(response.status).to eq(422)
        expect(service.reload.name).to eq(service_name)
        expect(config.reload.starting_page_path).to eq(config_starting_page_path)
        expect(identity.reload.username).to eq(identity_username)
      end

      it 'does not allow the type to change' do
        expect(service.type).to eq('WebApplicationService')
        update_params = valid_configured_service_params
        update_params[:service][:type] = 'SshService'
        norad_put :update, update_params

        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        expect(service.reload.type).to eq('WebApplicationService')
      end
    end

    context 'as an organization reader' do
      it 'cannot update a service for a machine' do
        @_current_user.add_role :organization_reader, machine.organization
        norad_put :update, valid_configured_service_params
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider' do
      it 'cannot update a service for a machine' do
        norad_put :update, id: service.id, service: { port: 1234 }
        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot update a machine in organization' do
        @_current_user.add_role :organization_admin, machine_without_permissions.organization
        norad_put :update, valid_configured_service_params
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @service1 = create :service, machine: @machine1
      @service2 = create :service, machine: @machine2
    end

    context 'as an organization admin' do
      it 'deletes a service for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect { norad_delete :destroy, id: @service1 }.to change(@machine1.services, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader' do
      it 'cannot delete a service for a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect { norad_delete :destroy, id: @service1 }.to change(@machine1.services, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot delete a service for a machine' do
        @_current_user.add_role :organization_admin, @machine2.organization
        expect { norad_delete :destroy, id: @service1 }.to change(@machine1.services, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
