class AddCascadingDeleteToTestRepositories < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :security_containers, :security_test_repositories
    add_foreign_key :security_containers, :security_test_repositories, on_delete: :cascade
  end
end
