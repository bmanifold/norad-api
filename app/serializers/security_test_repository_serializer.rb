# frozen_string_literal: true

# == Schema Information
#
# Table name: security_test_repositories
#
#  id                 :integer          not null, primary key
#  host               :string
#  name               :string           not null
#  public             :boolean          default(FALSE)
#  username           :string
#  password_encrypted :string
#  official           :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_security_test_repositories_on_host  (host) UNIQUE
#  index_security_test_repositories_on_name  (name) UNIQUE
#

class SecurityTestRepositorySerializer < ActiveModel::Serializer
  attributes :name, :public, :official, :id, :host, :username, :created_at, :updated_at
  attribute :authorizer

  def authorizer
    object.authorizer.serialize scope
  end
end
