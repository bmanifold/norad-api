# frozen_string_literal: true

class SecurityTestRepositoryAuthorizer < ApplicationAuthorizer
  class << self
    # Any user can list repositories. Finer-level attribute controls are handled in the serializer
    def readable_by?(_user)
      true
    end

    # Any user can create a repository
    def creatable_by?(_user)
      true
    end
  end

  # Any user can retrieve a repository. Finer-level attribute controls are handled in the serializer
  def readable_by?(_user)
    true
  end

  # Only this repository admin can update this repository
  def updatable_by?(user)
    admin?(user)
  end

  # Only this repository admin can delete this repository
  def deletable_by?(user)
    admin?(user)
  end

  # Determines if user can read all repository attributes
  def perusable_by?(user)
    resource.public || admin?(user) || reader?(user)
  end

  def admin?(user)
    user.has_role? :security_test_repository_admin, resource
  end

  def reader?(user)
    user.has_role? :security_test_repository_reader, resource
  end

  def serializer(user)
    if perusable_by?(user)
      SecurityTestRepositorySerializer
    else
      RedactedSecurityTestRepositorySerializer
    end
  end

  def serialize(user)
    {
      can_read?: readable_by?(user),
      can_peruse?: perusable_by?(user),
      can_edit?: updatable_by?(user)
    }
  end
end
