# frozen_string_literal: true

class EnforcementAuthorizer < ApplicationAuthorizer
  class << self
    def readable_by?(user, options)
      reader?(user, options[:in]) || admin?(user, options[:in])
    end

    def creatable_by?(user, options)
      admin?(user, options[:in])
    end
  end

  def deletable_by?(user)
    admin?(user, resource.organization)
  end
end
