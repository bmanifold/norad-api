# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

class RelayOfflineError < RelayError
  MESSAGE = 'Primary Relay is offline!'

  class << self
    def check(org, _options = {})
      org.primary_relay&.offline? ? create_error(org) : remove_error(org)
    end

    def message
      MESSAGE
    end
  end

  def message
    MESSAGE
  end
end
