# frozen_string_literal: true

module V1
  class ResultExportQueuesController < ApplicationController
    before_action :set_result_export_queue, only: %i[show update destroy]

    def index
      authorize_action_for ResultExportQueue, in: organization
      result_export_queues =
        ResultExportQueue.where(organization: organization).all

      render json: result_export_queues
    end

    def show
      authorize_action_for @result_export_queue
      render json: @result_export_queue
    end

    def update
      authorize_action_for @result_export_queue
      if @result_export_queue.update(result_export_queue_params)
        render json: @result_export_queue
      else
        render_errors_for(@result_export_queue)
      end
    end

    def destroy
      authorize_action_for @result_export_queue
      @result_export_queue.destroy
    end

    def create
      authorize_action_for ResultExportQueue, in: organization
      child_create
    end

    private

    def set_result_export_queue
      @result_export_queue ||= ResultExportQueue.find(params[:id])
    end

    def organization
      @organization ||= Organization.find(params[:organization_id])
    end

    def result_export_queue_creation_params
      result_export_queue_params.merge(created_by: current_user.uid, organization_id: organization.id)
    end

    def result_export_queue_params
      raise NotImplementedError, 'must be implemented by a child class'
    end

    def child_create
      raise NotImplementedError, 'must be implemented by a child class'
    end
  end
end
