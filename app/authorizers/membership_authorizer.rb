# frozen_string_literal: true

class MembershipAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user, options)
    admin?(user, options[:in])
  end

  def self.readable_by?(user, options)
    reader?(user, options[:in]) || admin?(user, options[:in])
  end

  def readable_by?(user)
    reader?(user, org) || admin?(user, org)
  end

  def deletable_by?(user)
    admin?(user, org) && (!admin?(resource.user, org) || resource.organization.admins.count > 1)
  end

  private

  def org
    resource.organization
  end
end
