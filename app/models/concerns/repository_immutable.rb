# frozen_string_literal: true

module RepositoryImmutable
  extend ActiveSupport::Concern

  included do
    private

    def prevent_official_changes
      return true unless official || official_changed?
      errors.add(:base, "Official Norad Repository can't be modified, removed, or whitelisted")
      throw :abort
    end
  end
end
