class AddDiscoveredColumnToServices < ActiveRecord::Migration[4.2]
  def change
    add_column :services, :discovered, :boolean, null: false, default: false
  end
end
