# frozen_string_literal: true

RSpec.shared_examples 'Scan Job Utilities' do
  let(:secret) { build_stubbed(:security_container_secret) }

  describe '#to_h' do
    before(:each) { allow(SecurityContainerSecret).to receive(:create!).and_return(secret) }

    it 'returns its attributes in hash form' do
      h = scan_job.to_h
      expect(h[:container_secret]).to eq(secret.secret)
      expect(h[:container_full_path]).to eq(container.full_path)
    end

    it 'merges in the Relay options' do
      relay = create(:docker_relay, organization: organization)
      relay.update_column(:verified, true)
      h = scan_job.to_h
      expect(h[:relay_secret]).to_not be_nil
      expect(h[:relay_queue]).to_not be_nil
      expect(h[:relay_exchange]).to_not be_nil
    end
  end
end
